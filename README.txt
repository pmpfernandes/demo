
MatchBox Team
Adriana Fonseca N:42728
Débora Viegas N:41637
Miguel Pino N:43642
Pedro Fernandes N:43323
Sara Lidon N:43641

Link para o bitbucket: 
Link para a applicacao no google app engine: https://2-dot-matchbox-164212.appspot.com/

https://2-dot-matchbox-164212.appspot.com/rest/register/signup usando o json:
{
	"emailAddress":"nome.especifico@campus.fct.unl.pt",
	"password":"password"
}

https://2-dot-matchbox-164212.appspot.com/rest/login/signin usando o json:
{
	"emailAddress":"nome.especifico@campus.fct.unl.pt",
	"password":"password"
}
E este irá devolver um authtoken na consola do Postman que é o que vamos utilizar no próximo comando:
https://2-dot-matchbox-164212.appspot.com/rest/logout/signout
Exemplo de authtoken
{
  "emailAddress": "pmp.fernandes@campus.fct.unl.pt",
  "creationDate": "2017-04-11T21:28:00.315Z",
  "expirationDate": "2017-04-11T02:28:00.315Z",
  "verificationNumber": "c07c5eae-356e-4c23-83a3-c04f222bef92"
}