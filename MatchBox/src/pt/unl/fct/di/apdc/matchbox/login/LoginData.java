package pt.unl.fct.di.apdc.matchbox.login;

public class LoginData {

	private String emailAddress;
	private String password;

	/**
	 * Empty constructor to handle error situations.
	 */
	public LoginData() {
	}

	/**
	 * Creates a new login data with the provided information.
	 * @param emailAddress
	 *            the email address.
	 * @param password
	 *            the password.
	 */
	public LoginData(String emailAddress, String password) {
		this.emailAddress = emailAddress;
		this.password = password;
	}

	/**
	 * Returns the email address.
	 * @return the email address.
	 */
	public String getEmailAddress() {
		return this.emailAddress;
	}

	/**
	 * Returns the password.
	 * @return the password.
	 */
	public String getPassword() {
		return this.password;
	}

}