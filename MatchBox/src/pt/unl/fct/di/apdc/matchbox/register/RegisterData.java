package pt.unl.fct.di.apdc.matchbox.register;

public class RegisterData {

	// User identification data.
	private String firstName;
	private String middleName;
	private String lastName;
	private String birthDay;
	private String birthMonth;
	private String birthYear;
	private String gender;

	// User personal information.
	private String streetName;
	private String apartmentNumber;
	private String apartmentFloor;
	private String apartmentFloorLetter;
	private String neighborhoodName;
	private String primaryZipcode;
	private String secondaryZipcode;
	private String zipcodeArea;
	private String citizenCardNumber;
	private String taxCardNumber;
	private String cellphoneNumber;
	private String telephoneNumber;

	// User login information.
	private String emailAddress;
	private String password;
	private String passwordConfirmation;

	/**
	 * Empty constructor to handle error situations.
	 */
	public RegisterData() {}

	/**
	 * Creates a new register data with the provided information.
	 * @param firstName
	 *            the user first name.
	 * @param middleName
	 *            the user middle name.
	 * @param lastName
	 *            the user last name.
	 * @param birthDay
	 *            the user birthday.
	 * @param birthMonth
	 *            the user birthmonth.
	 * @param birthYear
	 *            the user birthyear.
	 * @param gender
	 *            the user gender.
	 * @param streetName
	 *            the user street name.
	 * @param apartmentNumber
	 *            the user apartment number.
	 * @param apartmentFloor
	 *            the user apartment floor.
	 * @param apartmentFloorLetter
	 *            the user apartment floor letter.
	 * @param neighborhoodName
	 *            the user neighborhood name.
	 * @param primaryZipcode
	 *            the user first four digits of the zipcode.
	 * @param secondaryZipcode
	 *            the user last three digits of the zipcode.
	 * @param zipcodeArea
	 *            the user zipcode area.
	 * @param citizenCardNumber
	 *            the user citizen card number.
	 * @param taxCardNumber
	 *            the user tax card number.
	 * @param cellphoneNumber
	 *            the user cellphone number.
	 * @param telephoneNumber
	 *            the user telephone number.
	 * @param emailAddress
	 *            the user email address.
	 * @param password
	 *            the user password.
	 * @param passwordConfirmation
	 *            the user password confirmation.
	 */
	public RegisterData(String firstName, String middleName, String lastName,
			String birthDay, String birthMonth, String birthYear, String gender, String streetName,
			String apartmentNumber, String apartmentFloor, String apartmentFloorLetter, String neighborhoodName,
			String primaryZipcode, String secondaryZipcode, String zipcodeArea, String citizenCardNumber,
			String taxCardNumber, String cellphoneNumber, String telephoneNumber, String emailAddress, String password,
			String passwordConfirmation) {

		// User identification data.
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.birthDay = birthDay;
		this.birthMonth = birthMonth;
		this.birthYear = birthYear;
		this.gender = gender;

		// User personal information.
		this.streetName = streetName;
		this.apartmentNumber = apartmentNumber;
		this.apartmentFloor = apartmentFloor;
		this.apartmentFloorLetter = apartmentFloorLetter;
		this.neighborhoodName = neighborhoodName;
		this.primaryZipcode = primaryZipcode;
		this.secondaryZipcode = secondaryZipcode;
		this.zipcodeArea = zipcodeArea;
		this.citizenCardNumber = citizenCardNumber;
		this.taxCardNumber = taxCardNumber;
		this.cellphoneNumber = cellphoneNumber;
		this.telephoneNumber = telephoneNumber;

		// User login information.
		this.emailAddress = emailAddress;
		this.password = password;
		this.passwordConfirmation = passwordConfirmation;

	}

	/**
	 * Returns the user first name.
	 * @return the user first name.
	 */
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 * Returns the user middle name.
	 * @return the user middle name.
	 */
	public String getMiddleName() {
		return this.middleName;
	}

	/**
	 * Returns the user last name.
	 * @return the user last name.
	 */
	public String getLastName() {
		return this.lastName;
	}

	/**
	 * Returns the user birth day.
	 * @return the user birth day.
	 */
	public String getBirthDay() {
		return this.birthDay;
	}

	/**
	 * Returns the user birth month.
	 * @return the user birth month.
	 */
	public String getBirthMonth() {
		return this.birthMonth;
	}

	/**
	 * Returns the user birth year.
	 * @return the user birth year.
	 */
	public String getBirthYear() {
		return this.birthYear;
	}

	/**
	 * Returns the user gender.
	 * @return the user gender.
	 */
	public String getGender() {
		return this.gender;
	}

	/**
	 * Returns the user street name.
	 * @return a street name.
	 */
	public String getStreetName() {
		return this.streetName;
	}

	/**
	 * Returns the user apartment number.
	 * @return an apartment number.
	 */
	public String getApartmentNumber() {
		return this.apartmentNumber;
	}

	/**
	 * Returns an apartment floor.
	 * @return an apartment floor.
	 */
	public String getApartmentFloor() {
		return this.apartmentFloor;
	}

	/**
	 * Returns an apartment floor letter.
	 * @return an apartment floor letter.
	 */
	public String getApartmentFloorLetter() {
		return this.apartmentFloorLetter;
	}

	/**
	 * Returns a neighborhood name.
	 * @return a neighborhood name.
	 */
	public String getNeighborhoodName() {
		return this.neighborhoodName;
	}

	/**
	 * Returns a primary zipcode.
	 * @return a primary zipcode.
	 */
	public String getPrimaryZipcode() {
		return this.primaryZipcode;
	}

	/**
	 * Returns a secondary zipcode.
	 * @return a secondary zipcode.
	 */
	public String getSecondaryZipcode() {
		return this.secondaryZipcode;
	}

	/**
	 * Returns a zipcode area name.
	 * @return a zipcode area name.
	 */
	public String getZipcodeAreaName() {
		return this.zipcodeArea;
	}

	/**
	 * Returns the user citizen card number.
	 * @return the user citizen card number.
	 */
	public String getCitizenCardNumber() {
		return this.citizenCardNumber;
	}

	/**
	 * Returns the user tax card number.
	 * @return the user tax card number.
	 */
	public String getTaxCardNumber() {
		return this.taxCardNumber;
	}

	/**
	 * Returns the user cellphone number.
	 * @return the user cellphone number.
	 */
	public String getCellphoneNumber() {
		return this.cellphoneNumber;
	}

	/**
	 * Returns the user telephone number.
	 * @return the user telephone number.
	 */
	public String getTelephoneNumber() {
		return this.telephoneNumber;
	}

	/**
	 * Returns the user email address.
	 * @return the user email address.
	 */
	public String getEmailAddress() {
		return this.emailAddress;
	}

	/**
	 * Returns the user password.
	 * @return the user password.
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * Returns the user password confirmation.
	 * @return the user password confirmation
	 */
	public String getPasswordConfirmation() {
		return this.passwordConfirmation;
	}

}