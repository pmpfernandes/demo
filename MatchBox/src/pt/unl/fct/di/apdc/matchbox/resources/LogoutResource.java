package pt.unl.fct.di.apdc.matchbox.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;

import pt.unl.fct.di.apdc.matchbox.login.LoginToken;
import pt.unl.fct.di.apdc.matchbox.login.TokenValidation;

@Path("/logout")
public class LogoutResource {

	private static final String ROOT_ENTITY = "User";
	private static final String AUTH_TOKEN = "UserToken";
	private static final String MAGIC_PROPERTY = "token_magic";
	private static final String EXPIRED_PROPERTY = "token_expired";
	private static final TokenValidation TOKEN_VALIDATION = new TokenValidation();
	private static final DatastoreService DATASTORE = DatastoreServiceFactory.getDatastoreService();

	public LogoutResource() {}

	@POST
	@Path("/signout")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doLogout(LoginToken token) {
		// Melhor guardar x logouts request e depois mandar tudo para a datastore
		// Como depois ver se estao invalidos senao n foram enviados para a datastore
		if (token != null) {
			String emailAddress = token.getEmailAddress();
			String verificationNumber = token.getVerificationNumber();
			if (TOKEN_VALIDATION.validateAuthToken(emailAddress, verificationNumber)) {
				Transaction t = null;
				try {
					t = DATASTORE.beginTransaction();
					Key userKey = KeyFactory.createKey(ROOT_ENTITY, emailAddress);
					List<Entity> queryResult = DATASTORE.prepare(new Query(AUTH_TOKEN).setAncestor(userKey))
							.asList(FetchOptions.Builder.withDefaults());
					if (!queryResult.isEmpty()) {
						Entity authToken = queryResult.get(0);
						String tokenVerificationNumber = (String) authToken.getProperty(MAGIC_PROPERTY);
						if (tokenVerificationNumber.equals(verificationNumber)) {
							authToken.setUnindexedProperty(EXPIRED_PROPERTY, Boolean.TRUE.toString());
							DATASTORE.put(t, authToken); // TODO vai colocar as entradas duplicadas na datastore?
							t.commit();
							return Response.status(Status.OK).build();
						}
					}
				} catch (Exception e) {
					return Response.status(Status.INTERNAL_SERVER_ERROR).build();
				} finally {
					if (t != null && t.isActive()) {
						t.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).build();
					}
				}
			}
		}
		return Response.status(Status.BAD_REQUEST).build();
	}

}