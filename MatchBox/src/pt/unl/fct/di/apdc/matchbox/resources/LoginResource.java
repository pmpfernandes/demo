package pt.unl.fct.di.apdc.matchbox.resources;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pt.unl.fct.di.apdc.matchbox.login.LoginData;
import pt.unl.fct.di.apdc.matchbox.login.LoginToken;

@Path("/login")
public class LoginResource {

	private static final DatastoreService DATASTORE = DatastoreServiceFactory.getDatastoreService();
	private static final Gson JSON = new GsonBuilder().setPrettyPrinting().create();

	public LoginResource() {}

	@POST
	@Path("/signin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLogin(LoginData data, @Context HttpServletRequest request, @Context HttpHeaders headers) {
		Transaction t = null;
		try {
			t = DATASTORE.beginTransaction();
			LoginToken token = new LoginToken(data.getEmailAddress());
			Key userKey = KeyFactory.createKey("User", data.getEmailAddress());
			Entity user = DATASTORE.get(userKey);
			Entity userToken = new Entity("UserToken", user.getKey());
			userToken.setProperty("token_owner", data.getEmailAddress());
			userToken.setProperty("token_creation_date", token.getCreationDate());
			userToken.setProperty("token_expiration_date", token.getExpirationDate());
			userToken.setProperty("token_magic", token.getVerificationNumber());
			userToken.setProperty("token_expired", Boolean.FALSE.toString());
			DATASTORE.put(t, userToken);
			t.commit();
			return Response.ok().entity(JSON.toJson(token)).build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		} finally {
			if (t != null && t.isActive()) {
				t.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}
	}

}