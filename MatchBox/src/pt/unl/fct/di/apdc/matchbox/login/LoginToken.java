package pt.unl.fct.di.apdc.matchbox.login;

import java.util.UUID;

import org.joda.time.LocalTime;

public class LoginToken {

	private String emailAddress;
	private String creationDate;
	private String expirationDate;
	private String verificationNumber;

	/**
	 * Empty constructor to handle error situations.
	 */
	public LoginToken() {
	}

	/**
	 * Creates a new authentication token.
	 * @param emailAddress
	 *            the email address.
	 */
	public LoginToken(String emailAddress) {
		LocalTime time = new LocalTime();
		this.emailAddress = emailAddress;
		this.creationDate = time.toDateTimeToday().toString();
		this.expirationDate = time.plusHours(5).toDateTimeToday().toString();
		this.verificationNumber = UUID.randomUUID().toString();
	}

	/**
	 * Returns the email address.
	 * @return the email address.
	 */
	public String getEmailAddress() {
		return this.emailAddress;
	}

	/**
	 * Returns the creation date.
	 * @return the creation date.
	 */
	public String getCreationDate() {
		return this.creationDate;
	}

	/**
	 * Returns the expiration date.
	 * @return the expiration date.
	 */
	public String getExpirationDate() {
		return this.expirationDate;
	}

	/**
	 * Returns the verification number.
	 * @return the verification number.
	 */
	public String getVerificationNumber() {
		return this.verificationNumber;
	}

}