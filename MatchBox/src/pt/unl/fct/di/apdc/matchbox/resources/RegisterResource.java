package pt.unl.fct.di.apdc.matchbox.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.LocalDate;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;

import pt.unl.fct.di.apdc.matchbox.login.LoginData;

@Path("/register")
public class RegisterResource {

	private static final DatastoreService DATASTORE = DatastoreServiceFactory.getDatastoreService();

	@POST
	@Path("/signup")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	// Basic code
	public Response doRegistrationV1(LoginData data) {

		Entity user = new Entity("User", data.getEmailAddress());
		user.setProperty("user_pwd", DigestUtils.sha512Hex(data.getPassword()));
		user.setUnindexedProperty("user_creation_time", new LocalDate().toDate());
		DATASTORE.put(user);
		return Response.ok("{}").build();

	}

}